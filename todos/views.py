from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def list_view(request):
    todo_list = TodoList.objects.all()
    context = {
        "list": todo_list,
    }
    return render(request, "list/list.html", context)


def detail_view(request, id):
    list_set = get_object_or_404(TodoList, id=id)
    items = list_set.items.all()

    context = {
        "items": items,
        "list_set": list_set,
    }

    return render(request, "list/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]

            todo = TodoList.objects.create(name=name)
            todo.save()
            new_id = todo.id

            return redirect("todo_list_detail", new_id)

    else:
        form = TodoListForm()

        context = {"form": form, "type": "Create"}

        return render(request, "list/create.html", context)


def todo_list_edit(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)
        if form.is_valid():
            name = form.cleaned_data["name"]

            post.name = name
            post.save()

            return redirect("todo_list_detail", id)

    else:
        form = TodoListForm(instance=post)
        context = {"form": form, "type": "Update"}

        return render(request, "list/create.html", context)


def todo_list_delete(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        post.delete()
        return redirect("todo_list_list")

    return render(request, "list/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            id = item.list.id
            item.save()

            return redirect("todo_list_detail", id)

    else:
        form = TodoItemForm()
        context = {"form": form, "name": "", "type": "Create"}
        return render(request, "list/create_item.html", context)


def todo_item_edit(request, id):
    item = get_object_or_404(TodoItem, id=id)
    parent_id = item.list.id

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=parent_id)

    else:
        form = TodoItemForm(instance=item)
        parent_name = item.list.name
        context = {"form": form, "name": parent_name, "type": "Update"}
        return render(request, "list/create_item.html", context)
