from django.urls import path
from todos.views import (
    list_view,
    detail_view,
    todo_list_create,
    todo_list_edit,
    todo_list_delete,
    todo_item_create,
    todo_item_edit,
)

urlpatterns = [
    path("", list_view, name="todo_list_list"),
    path("<int:id>/", detail_view, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_edit, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_edit, name="todo_item_update"),
]
